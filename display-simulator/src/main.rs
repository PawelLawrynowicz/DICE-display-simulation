#![no_std]
use core::{fmt::Write, usize};
use embedded_graphics::{
    egline, egtext, primitive_style, primitives::Line, style::Styled, text_style,
};
use embedded_graphics::{egtriangle, fonts::Font6x8};
use embedded_graphics::{pixelcolor::BinaryColor, style::PrimitiveStyle};
use embedded_graphics::{prelude::*, primitives::Triangle};
use embedded_graphics_simulator::{
    BinaryColorTheme, OutputSettingsBuilder, SimulatorDisplay, Window,
};
use heapless::consts::*;
use heapless::FnvIndexMap;
use heapless::String;
use heapless::Vec;

const SINGLE_DISPLAY_RESOLUTION: (u32, u32) = (32, 32);
trait DrawableCrypto {
    fn draw_crypto(
        &mut self,
        symbol: String<U8>,
        price: f32,
        change: f32,
        font_size: (i32, i32),
        region: usize,
    );
    fn draw_wallet(&mut self, wallet_value: f32, daily_change: f32);
}

trait DrawablePrimitives {
    /// Primitive drawing methods for non-text objects on the screen
    fn draw_down_arrow(&mut self, x: i32, y: i32);
    fn draw_up_arrow(&mut self, x: i32, y: i32);
    fn draw_line(&mut self, x: i32, y: i32);
}

pub trait DrawHelpers {
    /// Helper methods, parsers, etc.
    fn ftof_precision(num: f32) -> f32;
    fn compare_tick_to_24h(
        crypto_map_tick: FnvIndexMap<String<U8>, f32, U32>,
        crypto_map_24h: FnvIndexMap<String<U8>, f32, U32>,
    ) -> FnvIndexMap<String<U8>, (f32, f32), U32>;
}

struct Screen {
    display: SimulatorDisplay<BinaryColor>,
    display_regions: Vec<(usize, usize), U32>,
}

impl Screen {
    pub fn new(
        display: SimulatorDisplay<BinaryColor>,
        screen_width: u32,
        screen_height: u32,
        font_size: (u32, u32),
    ) -> Self {
        let mut screen = Screen {
            display: SimulatorDisplay::new(Size::new(screen_width, screen_height)),
            display_regions: Vec::<(usize, usize), U32>::new(),
        };
        screen.generate_regions(screen_width, screen_height, font_size);
        screen
    }
    fn generate_regions(&mut self, screen_width: u32, screen_height: u32, font_size: (u32, u32)) {
        let mut regions = Vec::<(usize, usize), U32>::new();
        let min_vertical_space = (font_size.1 * 2);
        let mut min_horizontal_space = (7 + 9 * font_size.0) / SINGLE_DISPLAY_RESOLUTION.0;

        min_horizontal_space = min_horizontal_space * SINGLE_DISPLAY_RESOLUTION.0;
        if (7 + 9 * font_size.0) % SINGLE_DISPLAY_RESOLUTION.0 != 0 {
            min_horizontal_space += SINGLE_DISPLAY_RESOLUTION.0;
        }

        for i in 0..(screen_height / min_vertical_space) {
            for j in 0..(screen_width / min_horizontal_space) {
                regions.push((
                    (j * min_horizontal_space) as usize,
                    (i * min_vertical_space) as usize,
                ));
            }
        }

        self.display_regions = regions;
    }
}

impl DrawablePrimitives for Screen {
    /// Draws a red arrow aimed downwards with base from x to y, looks best if y-x is odd
    /// *`x` - base beginning
    /// *`y` - base end
    fn draw_down_arrow(&mut self, x: i32, y: i32) {
        let triangle: Styled<Triangle, PrimitiveStyle<BinaryColor>> = egtriangle!(
            points = [(x, y + 3), (x + 2, y + 5), (x + 4, y + 3)],
            style = primitive_style!(stroke_color = BinaryColor::On, fill_color = BinaryColor::On)
        );
        triangle.draw(&mut self.display);
    }
    /// Draws a green arrow aimed upwards with base from x to y, looks best if y-x is odd
    /// *`x` - base beginning
    /// *`y` - base end
    fn draw_up_arrow(&mut self, x: i32, y: i32) {
        let triangle: Styled<Triangle, PrimitiveStyle<BinaryColor>> = egtriangle!(
            points = [(x, y + 3), (x + 2, y + 1), (x + 4, y + 3)],
            style = primitive_style!(stroke_color = BinaryColor::On, fill_color = BinaryColor::On)
        );
        triangle.draw(&mut self.display);
    }
    /// Draws a line for x to y
    /// *`x` - beginning of the line
    /// *`y` - end of the line (that's what Graves says when you ban him)
    fn draw_line(&mut self, x: i32, y: i32) {
        let line: Styled<Line, PrimitiveStyle<BinaryColor>> = egline!(
            start = (x, y + 3),
            end = (x + 4, y + 3),
            style = primitive_style!(stroke_color = BinaryColor::On)
        );
        line.draw(&mut self.display);
    }
}

impl DrawableCrypto for Screen {
    /// Draws cryptocurrency in screen regions, position of elements inside the region are predetermined.
    /// *`symbol` - cryptocurrency symbol ("BTC", "ETH", etc.)
    /// *`price` - cryptocurrency value (currently only in USD)
    /// *`change` - current/daily value expressed in %.
    /// *`font_size` - font size
    fn draw_crypto(
        &mut self,
        symbol: String<U8>,
        price: f32,
        mut change: f32,
        font_size: (i32, i32),
        region: usize,
    ) {
        if self.display_regions.is_empty() {
            panic!("Display_regions empty Bruh");
        }
        let region = self.display_regions.get(region).unwrap();
        let (x, y) = (region.0 as i32, region.1 as i32);

        ///Handling edge cases
        let mut change_offset = font_size.1;
        if change > 9.99 || change < -9.99 {
            change_offset = 0;
        }
        if change > 99.99 {
            change = 99.9;
        } else if change < -99.99 {
            change = -99.9;
        }

        if change > 0.0 {
            self.draw_up_arrow(1 + x, y);
        } else if change < 0.0 {
            self.draw_down_arrow(1 + x, y);
        } else {
            DrawablePrimitives::draw_line(self, 1 + x, y)
        }
        let crypto_name = egtext!(
            text = &symbol,
            top_left = (7 + x, y),
            style = text_style!(
                font = Font6x8, // Font must to be the first styling property
                text_color = BinaryColor::On,
            )
        );
        let mut change_String = if change >= 0.0 {
            String::<U32>::from("+")
        } else {
            String::<U32>::from("")
        };
        core::write!(change_String, "{}", change);
        core::write!(change_String, "{}", "%");
        let change = egtext!(
            text = &change_String,
            top_left = (25 + change_offset + x, y),
            style = text_style!(
                font = Font6x8, // Font must to be the first styling property
                text_color = BinaryColor::On,
            )
        );

        let mut price_String = String::<U32>::from("$");
        core::write!(price_String, "{}", price);
        let price = egtext!(
            text = &price_String,
            top_left = (1 + x, font_size.1 + y),
            style = text_style!(
                font = Font6x8, // Font must to be the first styling property
                text_color = BinaryColor::On,
            )
        );
        crypto_name.draw(&mut self.display);
        change.draw(&mut self.display);
        price.draw(&mut self.display);
    }
    ///TODO:
    fn draw_wallet(&mut self, wallet_value: f32, daily_change: f32) {
        unimplemented!();
    }
}

impl DrawHelpers for Screen {
    /* This trait will become more useful if we decide to implement more customization methods */

    /// Transforms given float to a decimal number with double precision
    fn ftof_precision(num: f32) -> f32 {
        let temp_i: i32 = (num * 100.0) as i32;
        let float = temp_i as f32 / 100.0;
        float
    }
    // Compares current tick value to 24h average price and saves it to a hashmap
    // This method doesn't belong here but I had an idea for it so I made it
    fn compare_tick_to_24h(
        crypto_map_tick: FnvIndexMap<String<U8>, f32, U32>,
        crypto_map_24h: FnvIndexMap<String<U8>, f32, U32>,
    ) -> FnvIndexMap<String<U8>, (f32, f32), U32> {
        let mut change: f32 = 0.0;
        let mut tick_with_change = FnvIndexMap::<String<U8>, (f32, f32), U32>::new();
        for (symbol, price) in &crypto_map_tick {
            // CHANGE = (TICK-24H)*100/TICK [%] and with the decimal precision of 2
            //let temp_symbol: String<U8> = symbol.clone();
            change = Self::ftof_precision(
                (*price - crypto_map_24h.get(symbol).unwrap()) * (100.0) / *price,
            );
            tick_with_change.insert(symbol.clone(), (Self::ftof_precision(*price), change));
        }
        tick_with_change
    }
}

fn main() {
    let display: SimulatorDisplay<BinaryColor> = SimulatorDisplay::new(Size::new(192, 96));

    let mut screen = Screen::new(display, 192, 96, (6, 8));

    let output_settings = OutputSettingsBuilder::new()
        .theme(BinaryColorTheme::OledBlue)
        .build();
    let mut window = Window::new("DICE Project", &output_settings);
    let mut crypto_currency = populate_cryptos();

    let mut region = 0;

    for (symbol, (price, change)) in crypto_currency.into_iter() {
        screen.draw_crypto(symbol.clone(), *price, *change, (6, 8), region);
        region += 1;
    }

    window.show_static(&screen.display);
}

fn populate_cryptos() -> FnvIndexMap<heapless::String<U8>, (f32, f32), U32> {
    let mut cryptos = FnvIndexMap::<heapless::String<U8>, (f32, f32), U32>::new();

    let symbols = [
        "BTC", "ETH", "DOG", "ADA", "AVA", "BTT", "DAI", "EOS", "FIO", "GRT", "LTC", "SXP", "VET",
        "VIN", "XMR", "XTZ", "ZEC", "ZIL",
    ];
    let price = [
        45087.00, 3504.49, 123.33, 8.88, 12345.12, 123.87, 0.01, 234.65, 2406.13, 13245.80,
        6545.13, 123.57, 1987.22, 12.36, 76.34, 213.12, 2.23, 1.23,
    ];
    let change = [
        2.1, 0.0, -13.3, 1000.2, 23.4, 233.2, 7.1, -11.4, 99.9, 0.1, 10.2, 1.3, 6.4, 87.4, 3.5,
        -9999.9, -10.2, -1.2,
    ];
    for (i, sy) in symbols.iter().enumerate() {
        cryptos
            .insert(heapless::String::from(*sy), (price[i], change[i]))
            .unwrap();
    }

    cryptos
}
